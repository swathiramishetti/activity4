// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

#include "math.h"


InternetButton b = InternetButton();
int jmpcnt;
float jumps;
int perCompletd;

int initialZ;

void setup() {
    b.begin();
    jmpcnt = 0;
    jumps = 0;
    initialZ = b.readZ();
     Particle.function("totalJumps", jumpsgoal);
}

void loop(){

    if(jumps > 0)
    {
         int zValue = b.readZ();
   
        if(zValue > (initialZ + 50))
        {
               jmpcnt += 1;
            zValue = initialZ;

                     perCompletd = (jmpcnt / jumps) * 100;
                     Particle.publish("jmpCount", String(jmpcnt));
             delay(700);
             
             //turning on leds according to percentage
             for (int i = 1; i <= (perCompletd / 10); i++) {
                 b.ledOn(i, 0, 0, 255);
                }

              // if goal is complete, stop the loop
              if(perCompletd == 100)
              {
                  jumps = 0;
                  jmpcnt = 0;
                  perCompletd = 0;
                  delay(300);
                  b.allLedsOff();
              }
        }
    
    }
}
    //jump goal from mobile
    int jumpsgoal(String command)
    {
        if( jumps == 0 )
        {
        jumps = atof(command.c_str());
       
         Particle.publish("jumps", String(jumps));
        }
       
    }